package main

import (
	"path"

	"github.com/xanzy/go-gitlab"
)

func collectSubgroups(cli *gitlab.Client, group string, groups []string) ([]string, error) {
	subgroups, err := getGroupSubgroups(cli, group)
	if err != nil {
		return nil, err
	}
	if len(subgroups) == 0 {
		return groups, nil
	}
	for _, subgroup := range subgroups {
		subgroupName := path.Join(group, subgroup.Path)
		groups = append(groups, subgroupName)
		result, err := collectSubgroups(cli, subgroupName, groups)
		if err != nil {
			return nil, err
		}
		groups = result
	}
	return groups, nil
}

func getGroupSubgroups(cli *gitlab.Client, gid interface{}) ([]*gitlab.Group, error) {
	var groups []*gitlab.Group
	page := 1
	for {
		options := &gitlab.ListSubgroupsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
		}
		fetchedGroups, r, err := cli.Groups.ListSubgroups(gid, options, nil)
		if err != nil {
			return nil, err
		}
		for _, group := range fetchedGroups {
			groups = append(groups, group)
		}
		if r.NextPage <= 0 {
			break
		}
		page = r.NextPage
	}
	return groups, nil
}

func getGroupProjects(cli *gitlab.Client, gid interface{}) ([]*gitlab.Project, error) {
	var projects []*gitlab.Project
	page := 1
	for {
		options := &gitlab.ListGroupProjectsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
			OrderBy:          gitlab.String("updated_at"),
			IncludeSubgroups: gitlab.Bool(true),
			Archived:         gitlab.Bool(false),
		}
		fetchedProjects, r, err := cli.Groups.ListGroupProjects(gid, options, nil)
		if err != nil {
			return nil, err
		}
		for _, project := range fetchedProjects {
			projects = append(projects, project)
		}
		if r.NextPage <= 0 {
			break
		}
		page = r.NextPage
	}
	return projects, nil
}
