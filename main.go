package main

import (
	"flag"
	"log"
	"sort"

	"github.com/xanzy/go-gitlab"
)

var (
	token     = flag.String("token", "", "GitLab API Token")
	rootGroup = flag.String("group", "", "GitLab Group")
	baseURL   = flag.String("base-url", "https://gitlab.qleanlabs.ru/", "GitLab Base URL")
)

func main() {
	flag.Parse()

	cli, err := gitlab.NewClient(*token, gitlab.WithBaseURL(*baseURL))
	if err != nil {
		log.Fatal(err)
	}

	var groups []string
	groups, err = collectSubgroups(cli, *rootGroup, groups)
	if err != nil {
		log.Fatal(err)
	}
	groups = append(groups, *rootGroup)
	sort.Strings(groups)

	for gid, group := range groups {
		log.Printf("== %s [%d/%d]", group, gid+1, len(groups))
		projects, err := getGroupProjects(cli, group)
		if err != nil {
			log.Println(err)
			continue
		}
		for pid, project := range projects {
			log.Printf(" * %s [%d/%d]", project.PathWithNamespace, pid+1, len(projects))
			repos, _, err := cli.ContainerRegistry.ListRegistryRepositories(project.ID, &gitlab.ListRegistryRepositoriesOptions{
				ListOptions: gitlab.ListOptions{
					PerPage: 1,
				},
			})
			if err != nil {
				log.Println(err)
				continue
			}
			if len(repos) == 0 {
				continue
			}
			log.Printf(" ! %s\n", repos[0].Location)
		}
	}
}
