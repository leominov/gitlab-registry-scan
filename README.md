# gitlab-registry-scan

Поиск репозиториев и образами в Gitlab Container Registry.

По итогам последней проверки найдены:

```
cloud-platform/frontback/account
cloud-platform/frontback/kosmos
cloud-platform/frontback/orders
cloud-platform/gateway/backoffice
cloud-platform/gateway/notificator
cloud-platform/gateway/qlean
cloud-platform/services/account
cloud-platform/services/executors
cloud-platform/services/file-storage
cloud-platform/services/ids-exchanger
cloud-platform/services/leads
cloud-platform/services/mail-listener
cloud-platform/services/orders
cloud-platform/services/orders-qlean
cloud-platform/services/payment-invoice
cloud-platform/streamings/loader
cloud-platform/streamings/mango
cloud-platform/streamings/processing-engine
devops/letsencrypt
kosmos/booking
kosmos/ci-test
kosmos/kosmos-api
kosmos/kosmos-content-srv
kosmos/mobile/kosmos-customer-app
platform/sso/sso-identity-svs
platform/utils/curl
platform/utils/k8s-client
platform/utils/qrator-syslog
platform/vault-kv-sync
qleanlabs/qlean-api
```
